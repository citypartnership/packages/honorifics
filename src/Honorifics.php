<?php

namespace City\Honorifics;

use ReflectionClass;

abstract class Honorifics
{
    const Mr = 'Mr';
    const Mrs = 'Mrs';
    const Miss = 'Miss';
    const Ms = 'Ms';
    const AirCadre = 'Air Cdre';
    const Brgdr = 'Brgdr';
    const Capt = 'Capt';
    const Cmdr = 'Cmdr';
    const Col = 'Col';
    const Dame = 'Dame';
    const Dr = 'Dr';
    const FltLt = 'Flt Lt';
    const GroupCapt = 'Group Capt';
    const Judge = 'Judge';
    const Lady = 'Lady';
    const Lord = 'Lord';
    const LtCmdr = 'Lt-Cmdr';
    const LtCol = 'Lt-Col';
    const MajGen = 'Maj-Gen';
    const Major = 'Major';
    const Prof = 'Prof';
    const RearAdmrl = 'Rear Admrl';
    const Rev = 'Rev';
    const RevdCanon = 'Revd Canon';
    const RevdFather = 'Revd Father';
    const RtHonLord = 'Rt Hon Lord';
    const Sir = 'Sir';
    const TheHon = 'The Hon';
    const TheHonMrs = 'The Hon Mrs';
    const Viscount = 'Viscount';
    const WngCmdr = 'Wng Cmdr';
    const PerRepOf = 'Per Rep of';
    const PersRepsOf = 'Pers Rep of';
    const AdmorOf = 'Admor of';
    const AdmorsOf = 'Admors of';
    const ExorOf = 'Exor of';
    const ExorsOf = 'Exors of';

    /**
     * @return array
     */
    public static function getConstList() : array
    {
        $honorifics = array_values((new ReflectionClass(self::class))->getConstants());

        return array_combine($honorifics, $honorifics);
    }

    /**
     * @return string[]
     */
    public static function get() : array
    {
        return static::getConstList();
    }

    /**
     * @param string $delimiter
     *
     * @return string
     */
    public static function getAsString(string $delimiter = ',') : string
    {
        return implode($delimiter, static::get());
    }
}
