<?php

require_once 'vendor/autoload.php';

dd(
    \City\Honorifics\Honorifics::getConstList(), 
    \City\Honorifics\Honorifics::get(), 
    \City\Honorifics\Honorifics::getAsString()
);