# City - Honorifics Package

## Installation

Add the following to the `composer.json` of your *project*: 

```json
{
    // ...
    "require": {
        // ...
        "citypartnership/honorifics":"^1.0"
    },
  	"repositories": [
        {
            "type": "vcs",
            "url":  "https://gitlab.com/citypartnership/packages/honorifics"
        }
    ]
}
```



Then run `composer update`



**Note**: The `repositories` definition must be declared in the root `composer.json` - [see Composer docs](https://getcomposer.org/doc/04-schema.md#repositories) 



## Available Methods

`get()` 

Returns an array of all defined titles. 



`getAsString(string $delimiter = ',')`

Return all titles separated by the given $delimiter. 

